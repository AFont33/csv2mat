import configparser
import datetime
import logging
import os
import pprint
import re
import traceback

import numpy as np
import scipy.io as sio

from time import gmtime, strftime
from SessionParams import SessionParams
import concurrent.futures

ROOT_PATH = os.getcwd()
CURRENT_TIME = strftime("%Y%m%d_%H%M%S", gmtime())

config = configparser.ConfigParser()
config.read('user_settings.ini')

logging.basicConfig(level=logging.INFO)

def date_regex(path):
    """
    This function accepts a path corresponding to a CSV file with the format:
    ANIMAL_TASK_DATE.csv and returns the DATE.
    """
    return re.search('[0-9]{8}-[0-9]{6}', path).group(0)
    

def compute_file_dict(path, rat_name, date_range=None):
    """
    Looks for the CSV files in the specified path, sorts them by date and returns
    either all of them or those which are between a range of dates specified
    as a tuple. Returns a dictionary with the rat name as key and the list of
    all sessions in a list of strings:
    {"Rat1": [list_of_files], "Rat3": [list_of_files] ... }  
    """

    def correct_date(date: str, lower: str, upper: str) -> bool:
        """
        Checks if the date is inside the specified range; in that case returns True.
        Otherwise, returns False.
        """

        if lower.lower() == 'start':
            case = date <= upper + '-235959'
        elif upper.lower() == 'end':
            case = date >= lower + '-000000'
        else:
            case = lower + '-000000' <= date <= upper + '-235959'

        return case

    def path_generator(path: str, pattern: str):
        """
        This method returns a generator object which recursively finds files in "path"
        which match (end with) the "pattern".
        """
        for root, _, file in os.walk(path):
            for f in file:
                if f.endswith(pattern):
                    yield os.path.join(root, f)

    # Initialize the return dictionaries:
    rat_dict = {rat: [] for rat in rat_name}
    # Populate the paths:
    for path in path_generator(path, ".csv"):
        file_name = re.search('LE\w+-\w+', path).group(0) #  Match from LE to .csv
        animal, *_, date = re.split("_", file_name) #  Grab rat name and date. Unused variable is the task.
        if animal in rat_dict:
            if date_range is None or correct_date(date, *date_range):
                rat_dict[animal].append(path)
    # Now we sort the paths by date and populate the dates dict:
    for rat, path_list in rat_dict.items():
        rat_dict[rat] = sorted(path_list, key=date_regex)

    return rat_dict

def compute_session_params(sessions_list: list):  
    
    total_session_params = []
    converted_files = []
    
    for (nn, session) in enumerate(sessions_list):
        session_date = date_regex(session)
        try:
            session_params = SessionParams(session).session_params
            logging.info(f"\tSession {nn+1} ({session_date}) OK.")
        except Exception as e: #  If the session is bad for some reason, skip it and save the traceback in a file
            logging.warning(f"\tSession {nn+1} ({session_date}) FAILED.")
            if not os.path.exists("logs"):
                os.mkdir("logs")
            with open(f'logs/log_{CURRENT_TIME}.txt', 'a+') as f:
                f.write(f"Log record for file {session_date}: \n")
                f.write(str(e))
                f.write(traceback.format_exc())
                f.write("-"*25 + "\n")
        else: 
            converted_files.append(session)
            total_session_params.append(session_params)
            
    return converted_files, total_session_params

def check_config() -> (str, str):
    
    try:
        rat_names = config['CONFIG']['RAT_NAMES'].split('-')
    except KeyError:
        logging.exception(f" Error: You should specify the rat names in the user_settings.ini file.")
    
    try:
        date_range = config['CONFIG']['DATE_RANGE'].split('-')
    except KeyError:
        logging.exception(f" Error: You should specify the date range in the user_settings.ini file.")
    
    try:
        config['USER']['EXPERIMENTER']
    except KeyError:
        logging.exception(f" Error: You should specify the experimenter name in the user_settings.ini file.")
    
    try:
        config['PATHS']['PATH_CSV']
    except KeyError:
        logging.exception(f" Error: You should specify the path to the CSVs in the user_settings.ini file.")
        
    try:
        config['PATHS']['PATH_SAVE']
    except KeyError:
        logging.exception(f" Error: You should specify the path to save folder in the user_settings.ini file.")   
        
    try:
        config['CONFIG']['PRINT_LIST']
    except KeyError:
        logging.exception(f" Error: You should specify the PRINT_LIST variable in the user_settings.ini file.")
    
    return date_range, rat_names

def main():

    date_range, rat_names = check_config()

    path_to_csvs = config['PATHS']['PATH_CSV']
    path_to_save = config['PATHS']['PATH_SAVE']
    experimenter = config['USER']['EXPERIMENTER']

    if date_range[0].lower() != 'all':
        file_dict = compute_file_dict(path_to_csvs, rat_names, date_range=date_range)
    else:
        file_dict = compute_file_dict(path_to_csvs, rat_names)
    if config['CONFIG'].getboolean('PRINT_LIST'):
        printer = pprint.PrettyPrinter(indent=4)
        printer.pprint(file_dict)
    else:    
        for (rat, files) in file_dict.items():
            number_of_sessions = len(files)
            if number_of_sessions:
                logging.info(f' Starting script for rat {rat}. Found {number_of_sessions} sessions inside the specified range.')
                converted_files, session_params = compute_session_params(files)
                if converted_files:
                    logging.info(f' All sessions done. Saving file for rat {rat}. (This may take a while...)')
                    session_names = np.array([re.search('\w+_\w+_[0-9]+-[0-9]+\.csv', session).group(0) for session in converted_files], dtype = object)
                    try:
                        os.chdir(path_to_save)
                    except FileNotFoundError:
                        os.mkdir(path_to_save)
                        os.chdir(path_to_save)
                        
                    if len(converted_files) == 1:
                        file_date = date_regex(converted_files[0])[0:8]
                        sio.savemat(f'{rat}PreProcessData{file_date}.mat', 
                            {'RatName': rat, 
                            'ExperimentSession': session_names, 
                            'BehaviorData': session_params, 
                            'Experimenter': experimenter}, 
                            oned_as = 'column')
                    else:
                        first_date, second_date = date_regex(converted_files[0])[0:8], date_regex(converted_files[-1])[0:8]
                        sio.savemat(f'{rat}PreProcessData{first_date}-{second_date}.mat', 
                            {'RatName': rat, 
                            'ExperimentSession': session_names, 
                            'BehaviorData': session_params, 
                            'Experimenter': experimenter}, 
                            oned_as = 'column')  
                else:
                    logging.warning(f" Files about rat {rat} couldn't be converted. Skipping to next or finishing.") 
            else:
                logging.warning(f" Files about rat {rat} are missing or not inside the specified range. Skipping to next or finishing.")
            print('-'*25)
            
            os.chdir(ROOT_PATH)


if __name__ == "__main__": main()
