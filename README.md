# csv2mat documentation

csv2mat is a Python tool that converts PyBPOD session data in CSV format to .mat files
which can be processed with the Matlab pipeline developed at the lab. The user can 
specify which animals to analyze and a date range and the script will produce
a single file for animal which will contain the information of all the requested sessions.

## How to use it

1. Download the script:

    `git clone https://AFont33@bitbucket.org/AFont33/csv2mat.git`

2. Open the user_settings.ini file and edit it with your preferences. The configuration
file contains the following variables: 

    + **PATH_CSV** is the folder where you will have the CSV files that need to be
    converted.
    + **PATH_SAVE** is the folder where you want to save the generated .mat files. If it doesn't exist, it will be generated.
    + **RAT_NAMES** is the list of subjects that you want to analyze. The names are separated
    by a dash (-) and have to match the names in the CSV files. For example: 
    *RAT_NAMES = LE33* (for a single animal) or *RAT_NAMES = LE33-LE34-LE35* (for multiple).
    + **DATE_RANGE** will be the range of dates that you want to convert. It accepts 
    multiple options: put a proper range to convert the sessions in between (for example, 
    *DATE_RANGE = 20180801-20180804* to convert sessions from 1st to 4th of August; note the
    format YYYYMMDD and the fact that both dates are separated by a dash); use the *start*
    and *end* keywords (for example, *DATE_RANGE = start-20180804* or *DATE_RANGE = 20180801-end*) or use the *all* keyword (*DATE_RANGE = all*).
    + **PRINT_LIST**, if set to true, will print to the standard output the list of files that have been
    found and that match the rest of variables. It can be used to check that the script
    is indeed grabbing the intended files. Note that if this variable is set to true,
    no file will be generated.
    + **EXPERIMENTER**, your name, as will be put inside the Matlab file.

3. Make sure that the CSV files are inside the specified folder and run the script:

    `python main.py`


