import logging

import numpy as np
import pandas as pd

from ast import literal_eval

class UnsupportedTask(Exception):
    pass

class SessionParams:

    def __init__(self, path):
        self._path = path
        self._dataframe = None
        self.session_params = None
        self._length = None
        self._tasktype = None
        # Read the CSV and initialize session_params:
        self._read_dataframe(self._path)
        self._parse_dataframe()

    def _read_dataframe(self, path):
        """
        Take the path of a CSV file and read it as a pandas dataframe.
        """
        try:
            dataframe = pd.read_csv(path, skiprows=6, sep=';')
        except FileNotFoundError:
            logging.critical("Error: CSV file not found. Exiting...")
            raise
        else:
            self._dataframe = dataframe

    def _parse_dataframe(self):
        
        def calculate_length(punish_data, reward_data, invalids=None):
            """
            Length (total trials) is calculated as the sum of invalids, corrects
            and incorrects.
            """
            if invalids is None:
                inner_length = (punish_data.dropna().size 
                                + reward_data.dropna().size)
            else:
                inner_length = (punish_data.dropna().size 
                                + reward_data.dropna().size 
                                + invalids.dropna().size)
            return inner_length
        
        def compute_hithistory(punish_data, invalids):
            """
            Computes the HitHistory vector: contains 1 for correct trials,
            0 for incorrect trials and -3 for invalid trials (old BControl
            convention).
            """
            hithistory = []    
            for (ii, elem) in enumerate(punish_data):
                if not invalids[ii]:
                    hithistory.append(-3)
                elif elem: hithistory.append(1)
                else: hithistory.append(0)
            return hithistory

        def obtain_task_type():
            accepted_tasks = {'p3', 'p4', 'pilot_s6_01'}
            self._tasktype = self._dataframe[self._dataframe.MSG == 'PROTOCOL-NAME']['+INFO'].iloc[0]
            if self._tasktype not in accepted_tasks:
                raise UnsupportedTask(f"{self._tasktype} is not a supported task.")
        obtain_task_type()        
        
        try:
            stage_number = self._dataframe[self._dataframe.MSG == 'STAGE_NUMBER']['+INFO'].iloc[0]        
        except IndexError:
            stage_number = -1
            
        punish_data = self._dataframe.query("TYPE=='STATE' and MSG=='Punish'")['BPOD-FINAL-TIME']
        invalids = self._dataframe.query("TYPE=='STATE' and MSG=='Invalid'")['BPOD-FINAL-TIME']
        reward_data = self._dataframe.query("TYPE=='STATE' and MSG=='Reward'")['BPOD-FINAL-TIME']
        if invalids.size:
            #  Now invalids will contain False for invalid trials and True otherwise:
            self._length = calculate_length(punish_data, reward_data, invalids)
            invalids = invalids.apply(lambda x: np.isnan(float(x))).values[:self._length]
        else:
            self._length = calculate_length(punish_data, reward_data)
            invalids = [True] * self._length
            #logging.warning("This session didn't take invalid trials into account.")
        assert self._length > 0, "Session results not found; report can't be generated. Exiting..."
        #  Now punish_data will contain True if the answer was correct, False if incorrect or invalid:
        punish_data = punish_data.apply(lambda x: np.isnan(float(x))).values[:self._length]
        #  reward_side contains a 1 if the correct answer was the (R)ight side, 0 otherwise:
        try:
            reward_side = self._dataframe[self._dataframe.MSG == "REWARD_SIDE"]['+INFO'].iloc[-1][1:-1].split(',')[:self._length]
        except IndexError: #  Compatibility with old files
            #logging.warning("REWARD_SIDE vector not found. Trying old VECTOR_CHOICE...")
            try:
                reward_side = self._dataframe[self._dataframe.MSG == 'VECTOR_CHOICE']['+INFO'].iloc[-1][1:-1].split(',')[:self._length]
            except IndexError:
                logging.critical("Neither REWARD_SIDE nor VECTOR_CHOICE found. Exiting...")
                raise

        reward_side = np.transpose(np.array([int(x) for x in reward_side], dtype=np.object))
        coherences = self._dataframe[self._dataframe['MSG'] == 'coherence01']['+INFO'].values[:self._length]
        coherences = coherences.astype(float)
        #  List of possible, unique coherences:
        coherence_vector = sorted(list(set(coherences)))
        #  Coherences for every trial, as indices of coherence_vector. Remember
        #  that Matlab indices start at 1:
        coherence_list = [coherence_vector.index(elem)+1 for elem in
                coherences]
        coherence_vector = np.array(coherence_vector)
        #  1 for L, 2 for R:
        rewardside_list = [1 if elem == 0 else 2 for elem in reward_side]
        hithistory = compute_hithistory(punish_data, invalids)       
        # left_envelopes contains a list of strings:
        left_envelopes = self._dataframe.query("TYPE=='VAL' and MSG=='left_envelope'")['+INFO'].values[:self._length]
        right_envelopes = self._dataframe.query("TYPE=='VAL' and MSG=='right_envelope'")['+INFO'].values[:self._length] 
        L = [np.array(list(map(float, elem[1:-1].split(',')))) for elem in left_envelopes]
        R = [np.array(list(map(float, elem[1:-1].split(',')))) for elem in right_envelopes]
        envelope_diff = np.transpose(np.array([elem + R[mm] for (mm, elem) in enumerate(L)])[np.newaxis])
        stimulus_ind = np.transpose(np.array(list(range(1, len(left_envelopes)+1)))[np.newaxis])
        stimulus_R = np.transpose(R) 
        stimulus_L = np.transpose(L) 
        
        if self._tasktype == 'p4' or self._tasktype == 'pilot_s6_01':
            prob_repeat = self._dataframe[self._dataframe['MSG'] == 'prob_repeat']['+INFO'].values[:self._length]
            if not prob_repeat.size: #  The session is of the old type, and has prob_repeat missing
                # so we compute it:
                prob_repeat = self._compute_prob_repeat()[:self._length] / 100
            prob_repeat = prob_repeat.astype(float)
            env_prob_repeat = list(prob_repeat)
        else: #  p3 or pilot
            env_prob_repeat = [0.5] * self._length #  for p3 tasks
            
        parsedEvents = np.transpose(self._parsed_events()[np.newaxis])

        self.session_params = {'CoherenceVec': coherence_vector, 
                            'RewardSideList': rewardside_list, 
                            'HitHistoryList': hithistory, 
                            'CoherenceList': coherence_list, 
                            'StimulusList': stimulus_ind,
                            'EnviroProbRepeat': env_prob_repeat, 
                            'ParsedEvents': parsedEvents,
                            'StageNumber': stage_number,
                            'StimulusBlock': envelope_diff,
                            'StimulusR': stimulus_R,
                            'StimulusL': stimulus_L} 
    
    def _compute_prob_repeat(self):
        """
        This function computes the probability of repeat of the blocks that form the session.
        It is used in those sessions which were p4 but which didn't have yet the data in the CSV
        (sessions before the 10th of august, 2018).
        """
        def getrep(trial_list, blen):
            nblocks = int(round(len(trial_list)/blen, 0))
            transitions= np.arange(nblocks, step=1)*blen
            segmentrepprobs=[]
            for item in transitions.tolist():
                segment = trial_list[item:item+blen]
                rep=0
                for j in range(1,blen):
                    if segment[j]==segment[j-1]:
                        rep+=1
                segmentrepprobs = segmentrepprobs+ [rep]
            thereps = np.array(segmentrepprobs)*100/(blen-1)
            first, second = np.arange(0, nblocks, step=2), np.arange(1,nblocks, step=2)

            if thereps[first].mean()>thereps[second].mean():
                probreporder= [80,20]
            else:
                probreporder=[20,80]
            if abs(thereps[first].mean()-thereps[second].mean())<20:
                print('beware, not rly different probs among blocks')
                
            return np.repeat(probreporder*int(nblocks/2),blen)

        bsize = int(self._dataframe.loc[(self._dataframe.TYPE=='VAL') & (self._dataframe.MSG=='VAR_BLEN'), '+INFO'].values[0])
        trial_list = literal_eval(self._dataframe.loc[(self._dataframe.TYPE=='VAL') & (self._dataframe.MSG=='REWARD_SIDE'), '+INFO'].values[0])

        return getrep(trial_list, bsize)


    @staticmethod
    def _cols(*argv):
        """
        Helper function. This method packs data into a vertical shape so that 
        Matlab recognizes it as a column vector.
        """
        if len(argv) == 2:
            columns = np.column_stack((argv[0], argv[1]))
        elif len(argv) == 1:
            columns = np.column_stack((argv[0], argv[0]))
        else:
            raise TypeError("This function accepts at most 2 arguments.")
        return columns

    def _timestamp_vectors(self, vec):
        """
        Helper function. This method takes a numpy array, returns a number of values
        equal to the trial length (self._length) and casts the values to float.
        """
        column = vec.values[:self._length].astype(float)
        return column


    def _parsed_events(self):
        """
        This method takes the session dataframe and parses the timestamps, converting from 
        PyBPOD state names to old BControl names. It returns a numpy array that will be
        later saved into the .mat file as a 'parsedEvents' cell.
        """
        
        trials_start = self._timestamp_vectors(self._dataframe.query("TYPE=='INFO' and MSG=='TRIAL-BPOD-TIME'")['BPOD-INITIAL-TIME'])
        trials_end = self._dataframe.query("TYPE=='INFO' and MSG=='TRIAL-BPOD-TIME'")['BPOD-FINAL-TIME'].values[:self._length].astype(float)
        trials_nan = np.array([np.nan] * self._length)

        #early_w_start = self._dataframe.query("TYPE=='STATE' and MSG=='Invalid'")['BPOD-INITIAL-TIME'].values[:self._length].astype(float) + trials_start
        #early_w_end = self._dataframe.query("TYPE=='STATE' and MSG=='Invalid'")['BPOD-FINAL-TIME'].values[:self._length].astype(float) + trials_start
        #early_w_start = np.array([elem if not np.isnan(elem) else [] for elem in list(early_w_start)], dtype=np.object)
        #early_w_end = np.array([elem if not np.isnan(elem) else [] for elem in list(early_w_end)], dtype=np.object)
        
        playTarget_start = self._dataframe.query("TYPE=='STATE' and MSG=='StartSound'")['BPOD-INITIAL-TIME'].values[:self._length].astype(float) + trials_start
        try:
            playTarget_end = self._dataframe.query("TYPE=='STATE' and MSG=='KeepSoundOn'")['BPOD-FINAL-TIME'].values[:self._length].astype(float) + trials_start
        except ValueError:
            playTarget_end = self._dataframe.query("TYPE=='STATE' and MSG=='StartSound'")['BPOD-FINAL-TIME'].values[:self._length].astype(float) + trials_start
        playTarget_start = np.array([elem if not np.isnan(elem) else [] for elem in list(playTarget_start)], dtype=np.object)
        playTarget_end = np.array([elem if not np.isnan(elem) else [] for elem in list(playTarget_end)], dtype=np.object)

        waitApoke_start = self._dataframe.query("TYPE=='STATE' and MSG=='WaitResponse'")['BPOD-INITIAL-TIME'].values[:self._length].astype(float) + trials_start
        waitApoke_end = self._dataframe.query("TYPE=='STATE' and MSG=='WaitResponse'")['BPOD-FINAL-TIME'].values[:self._length].astype(float) + trials_start
        waitApoke_start = np.array([elem if not np.isnan(elem) else [] for elem in list(waitApoke_start)], dtype=np.object)
        waitApoke_end = np.array([elem if not np.isnan(elem) else [] for elem in list(waitApoke_end)], dtype=np.object)

        reward_start = self._dataframe.query("TYPE=='STATE' and MSG=='Reward'")['BPOD-INITIAL-TIME'].values[:self._length].astype(float) + trials_start
        reward_end = self._dataframe.query("TYPE=='STATE' and MSG=='Reward'")['BPOD-FINAL-TIME'].values[:self._length].astype(float) + trials_start
        reward_start = np.array([elem if not np.isnan(elem) else [] for elem in list(reward_start)], dtype=np.object)
        reward_end = np.array([elem if not np.isnan(elem) else [] for elem in list(reward_end)], dtype=np.object)
        
        intertrial_interval_start = trials_end
        intertrial_interval_end = np.append(trials_start[1:], trials_end[-1])

        new_trial_indexes = self._dataframe.query("TYPE=='TRIAL' and MSG=='New trial'").index[:self._length]
        parsedEventsCell = np.zeros((self._length,), dtype=np.object)
        for jj in range(self._length):
            states = {'starting_state': 'state_0', 'ending_state': 'final_state'}

            if jj != self._length - 1:
                index_1, index_2 = new_trial_indexes[jj], new_trial_indexes[jj+1]
                trial_band_events = self._dataframe.query("TYPE=='EVENT' and index > @index_1 and index < @index_2")
                trial_band_states = self._dataframe.query("TYPE=='STATE' and index > @index_1 and index < @index_2")
            else:
                index_1 = new_trial_indexes[jj]
                trial_band_events = self._dataframe.query("TYPE=='EVENT' and index > @index_1")
                trial_band_states = self._dataframe.query("TYPE=='STATE' and index > @index_1")
            waitCpoke_start = trial_band_states.query("MSG=='WaitCPoke'")['BPOD-INITIAL-TIME'].values.astype(float) \
                        + trials_start[jj]
            waitCpoke_end = trial_band_states.query("MSG=='WaitCPoke'")['BPOD-FINAL-TIME'].values.astype(float) \
                            + trials_start[jj]
            
            preStimDelay_start = trial_band_states.query("MSG=='Fixation'")['BPOD-INITIAL-TIME'].values.astype(float) \
                                + trials_start[jj]
            preStimDelay_end = trial_band_states.query("MSG=='Fixation'")['BPOD-FINAL-TIME'].values.astype(float) \
                            + trials_start[jj]

            states['state_0'] = self._cols(trials_nan[jj], trials_end[jj])
            states['wait_for_apoke'] = self._cols(waitApoke_start[jj], waitApoke_end[jj])
            states['reward'] = self._cols(reward_start[jj], reward_end[jj])           
            states['check_next_trial_ready'] = self._cols(intertrial_interval_end[jj]) 
            states['play_target'] = self._cols(playTarget_start[jj], playTarget_end[jj])
            states['intertrial_interval'] = self._cols(intertrial_interval_start[jj], intertrial_interval_end[jj])
            states['final_state'] = states['check_next_trial_ready']
            states['wait_for_cpoke'] = self._cols(waitCpoke_start, waitCpoke_end)
            states['send_trial_info'] = self._cols(waitCpoke_start[0])
            states['pre_stim_delay'] = self._cols(preStimDelay_start, preStimDelay_end)
            a = []
            if states['pre_stim_delay'].size > 2:
                for mm in range(0, (states['pre_stim_delay'].size - 2) // 2):
                   a.append(states['pre_stim_delay'][mm, 1])
            b = np.array(a)
            states['early_withdrawal_pre'] = self._cols(b)
            
            found_L_in = found_C_in = found_R_in = found_L_out = found_C_out = found_R_out = False
            C_start = []
            L_start = []
            R_start = []
            C_end = []
            L_end = []
            R_end = []
            for (_, row) in trial_band_events.iterrows():
                if row['MSG'] == '68': # Port1In
                    found_L_in = True
                    if found_L_out:
                        L_start.append(np.nan)
                        found_L_out = False
                    L_start.append(row['BPOD-INITIAL-TIME'])
                elif row['MSG'] == '70': # Port2In
                    found_C_in = True
                    if found_C_out:
                        C_start.append(np.nan)
                        found_C_out = False
                    C_start.append(row['BPOD-INITIAL-TIME'])
                elif row['MSG'] == '72': # Port3In
                    found_R_in = True
                    if found_R_out:
                        R_start.append(np.nan)
                        found_R_out = False
                    R_start.append(row['BPOD-INITIAL-TIME'])
                elif row['MSG'] == '69': # Port1Out
                    if not found_L_in:
                        found_L_out = True
                    else:
                        found_L_in = False
                    L_end.append(row['BPOD-INITIAL-TIME'])
                elif row['MSG'] == '71': # Port2Out
                    if not found_C_in:
                        found_C_out = True
                    else:
                        found_C_in = False
                    C_end.append(row['BPOD-INITIAL-TIME'])
                elif row['MSG'] == '73': # Port3Out
                    if not found_R_in:
                        found_R_out = True
                    else:
                        found_R_in = False
                    R_end.append(row['BPOD-INITIAL-TIME'])
                else:
                    pass

            if found_L_in:
                L_end.append(np.nan)
            if found_C_in:
                C_end.append(np.nan)
            if found_R_in:
                R_end.append(np.nan)

            if found_L_out:
                L_start.append(np.nan)
            if found_C_out:
                C_start.append(np.nan)
            if found_R_out:
                R_start.append(np.nan)   
                

            C = np.column_stack((C_start + trials_start[jj], C_end + trials_start[jj])) 
            L = np.column_stack((L_start + trials_start[jj], L_end + trials_start[jj])) 
            R = np.column_stack((R_start + trials_start[jj], R_end + trials_start[jj])) 

            pokes = {'L': L, 'R': R, 'C': C}
            upper_struct = {'states': states, 'pokes': pokes}
            parsedEventsCell[jj] = upper_struct
            
        return parsedEventsCell
